﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretBluePrint_Controller : MonoBehaviour
{
    public int GetSellAmount()
    {
        TurretBluePrint_Model turretBluePrintModel = GetComponent<TurretBluePrint_Model>();
        return turretBluePrintModel.cost / 2;
    }
}
