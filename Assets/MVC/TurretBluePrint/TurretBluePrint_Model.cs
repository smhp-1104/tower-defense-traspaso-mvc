﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretBluePrint_Model : MonoBehaviour
{
    public GameObject prefab;
    public int cost;

    public GameObject upgradedPrefab;
    public int upgradeCost;
}
