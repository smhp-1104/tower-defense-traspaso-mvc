﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Waypoints_Controller : MonoBehaviour
{
    void Awake()
    {
        Waypoints_Model WaypointModel = GetComponent<Waypoints_Model>();

        WaypointModel.points = new Transform[transform.childCount];
        for (int i = 0; i < WaypointModel.points.Length; i++)
        {
            WaypointModel.points[i] = transform.GetChild(i);
        }
    }
}
