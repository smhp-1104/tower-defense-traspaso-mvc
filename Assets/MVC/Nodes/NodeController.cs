﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class NodeController : MonoBehaviour
{
    // Start is called before the first frame update

    public bool isUpgraded = false;
    public TurretBlueprint turretBlueprint;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnMouseDown()
    {
        BuildManagerModel buildManagerModel = GameObject.Find("GameMaster").GetComponent<BuildManagerModel>();
        BuildManagerController buildManagerController = GameObject.Find("GameMaster").GetComponent<BuildManagerController>();
        BuildManagerView buildManagerView = GameObject.Find("GameMaster").GetComponent<BuildManagerView>();
        NodeModel nodeModel = GetComponent<NodeModel>();
        if (EventSystem.current.IsPointerOverGameObject())
            return;

        if (nodeModel.turret != null)
        {
            buildManagerController.SelectNode(this);
            return;
        }

        if (!buildManagerModel.CanBuild)
            return;

        BuildTurret(buildManagerView.GetTurretToBuild());
    }

    void BuildTurret(TurretBlueprint blueprint)
    {
        PlayerStatsModel playerStatsModel = GameObject.Find("GameMaster").GetComponent<PlayerStatsModel>();
        NodeController nodeController = GetComponent<NodeController>();
        NodeModel nodeModel = GetComponent<NodeModel>();
        NodeView nodeView = GetComponent<NodeView>();

        if (playerStatsModel.Money < blueprint.cost)
        {
            nodeView.NotEnoughtBuild();
            return;
        }

        playerStatsModel.Money -= blueprint.cost;

        GameObject _turret = (GameObject)Instantiate(blueprint.prefab, nodeController.GetBuildPosition(), Quaternion.identity);
        nodeModel.turret = _turret;

        turretBlueprint = blueprint;

        GameObject effect = (GameObject)Instantiate(nodeModel.buildManager.buildEffect, nodeController.GetBuildPosition(), Quaternion.identity);
        Destroy(effect, 5f);

        nodeView.Enought();
    }

    public Vector3 GetBuildPosition()
    {
        NodeModel nodeModel = GetComponent<NodeModel>();
        return transform.position + nodeModel.positionOffset;
    }

    public void UpgradeTurret()
    {
        PlayerStatsModel playerStatsModel = GetComponent<PlayerStatsModel>();
        NodeModel nodeModel = GetComponent<NodeModel>();
        NodeView nodeView = GetComponent<NodeView>();
        if (playerStatsModel.Money < turretBlueprint.upgradeCost)
        {
            nodeView.NotEnoughtUpgrade();
            return;
        }

        playerStatsModel.Money -= turretBlueprint.upgradeCost;

        //Get rid of the old turret
        Destroy(nodeModel.turret);

        //Build a new one
        GameObject _turret = (GameObject)Instantiate(turretBlueprint.upgradedPrefab, GetBuildPosition(), Quaternion.identity);
        nodeModel.turret = _turret;

        GameObject effect = (GameObject)Instantiate(nodeModel.buildManager.buildEffect, GetBuildPosition(), Quaternion.identity);
        Destroy(effect, 5f);

        isUpgraded = true;

        Debug.Log("Turret upgraded!");
    }

    public void SellTurret()
    {
        PlayerStatsModel playerStatsModel = GetComponent<PlayerStatsModel>();
        NodeModel nodeModel = GetComponent<NodeModel>();
        playerStatsModel.Money += turretBlueprint.GetSellAmount();


        GameObject effect = (GameObject)Instantiate(nodeModel.buildManager.sellEffect, GetBuildPosition(), Quaternion.identity);
        Destroy(effect, 5f);

        Destroy(nodeModel.turret);
        turretBlueprint = null;
    }

    void OnMouseEnter()
    {
        BuildManagerModel buildManagerModel = GameObject.Find("GameMaster").GetComponent<BuildManagerModel>();
        NodeModel nodeModel = GetComponent<NodeModel>();
        NodeView nodeView = GetComponent<NodeView>();

        if (EventSystem.current.IsPointerOverGameObject())
            return;

        if (!buildManagerModel.CanBuild)
            return;

        if (buildManagerModel.HasMoney)
        {
            nodeView.HasMoney();
        }
        else
        {
            nodeView.NoMoney();
        }

    }

    void OnMouseExit()
    {
        NodeModel nodeModel = GetComponent<NodeModel>();
        nodeModel.rend.material.color = nodeModel.startColor;
    }
}
