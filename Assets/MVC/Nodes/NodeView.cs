﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NodeView : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        NodeModel nodeModel = GetComponent<NodeModel>();

        nodeModel.rend = GetComponent<Renderer>();
        nodeModel.startColor = nodeModel.rend.material.color;

        nodeModel.buildManager = BuildManager.instance;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void HasMoney()
    {
        NodeModel nodeModel = GetComponent<NodeModel>();
        nodeModel.rend.material.color = nodeModel.hoverColor;
    }

    public void NoMoney()
    {
        NodeModel nodeModel = GetComponent<NodeModel>();
        nodeModel.rend.material.color = nodeModel.notEnoughMoneyColor;
    }

    public void NotEnoughtBuild()
    {
        Debug.Log("Not enough money to build that!");
    }

    public void NotEnoughtUpgrade()
    {
        Debug.Log("Not enough money to build that!");
    }

    public void Enought()
    {
        Debug.Log("Not enough money to build that!");
    }



}                                                                                                                                               
