﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStatsController : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
        PlayerStatsModel playerStatsModel = GetComponent<PlayerStatsModel>();

        playerStatsModel.startMoney = 400;
        playerStatsModel.startLives = 20;

        playerStatsModel.Money = playerStatsModel.startMoney;
        playerStatsModel.Lives = playerStatsModel.startLives;

        playerStatsModel.Rounds = 0;



    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
