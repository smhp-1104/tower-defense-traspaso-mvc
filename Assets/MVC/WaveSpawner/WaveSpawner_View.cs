﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WaveSpawner_View : MonoBehaviour
{

    public void SpawnEnemy(GameObject enemy)
    {
        WaveSpawner_Model WaveSpawnerModel = GetComponent<WaveSpawner_Model>();
        Instantiate(enemy, WaveSpawnerModel.spawnPoint.position, WaveSpawnerModel.spawnPoint.rotation);
    }
}
