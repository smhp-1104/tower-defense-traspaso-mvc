﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WaveSpawner_Controller : MonoBehaviour
{

    void Update()
    {
        WaveSpawner_Model WaveSpawnerModel =  GetComponent<WaveSpawner_Model>();

        if (WaveSpawnerModel.EnemiesAlive > 0)
        {
            return;
        }

        if (WaveSpawnerModel.waveIndex == WaveSpawnerModel.waves.Length)
        {
            WaveSpawnerModel.gameManager.WinLevel();
            this.enabled = false;
        }

        if (WaveSpawnerModel.countdown <= 0f)
        {
            StartCoroutine(SpawnWave());
            WaveSpawnerModel.countdown = WaveSpawnerModel.timeBetweenWaves;
            return;
        }

        WaveSpawnerModel.countdown -= Time.deltaTime;

        WaveSpawnerModel.countdown = Mathf.Clamp(WaveSpawnerModel.countdown, 0f, Mathf.Infinity);

        WaveSpawnerModel.waveCountdownText.text = string.Format("{0:00.00}", WaveSpawnerModel.countdown);


    }



    IEnumerator SpawnWave()
    {
        WaveSpawner_Model WaveSpawnerModel = GetComponent<WaveSpawner_Model>();
        WaveSpawner_View WaveSpawnerView = GetComponent<WaveSpawner_View>();

        PlayerStats.Rounds++;

        Wave wave = WaveSpawnerModel.waves[WaveSpawnerModel.waveIndex];

        WaveSpawnerModel.EnemiesAlive = wave.count;

        for (int i = 0; i < wave.count; i++)
        {
            WaveSpawnerView.SpawnEnemy(wave.enemy);
            yield return new WaitForSeconds(1f / wave.rate);
        }

        WaveSpawnerModel.waveIndex++;
    }
}
