﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager_Controller : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        GameManager_Model GameManagerModel = GetComponent<GameManager_Model>();
        GameManagerModel.GameIsOver = false;
    }

    void Update()
    {
        GameManager_Model GameManagerModel = GetComponent<GameManager_Model>();
        if (GameManagerModel.GameIsOver)
            return;

        PlayerStatsModel PlayerStatsModel = GetComponent<PlayerStatsModel>();
        if (PlayerStatsModel.Lives <= 0)
        {
            EndGame();
        }
    }

    void EndGame()
    {
        GameManager_Model GameManagerModel = GetComponent<GameManager_Model>();
        GameManagerModel.GameIsOver = true;
        GameManagerModel.gameOverUI.SetActive(true);
    }

    public void WinLevel()
    {
        GameManager_Model GameManagerModel = GetComponent<GameManager_Model>();
        GameManagerModel.GameIsOver = true;
        GameManagerModel.completeLevelUI.SetActive(true);
    }

}

