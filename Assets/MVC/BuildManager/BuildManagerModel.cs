﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildManagerModel : MonoBehaviour
{
    // Start is called before the first frame update

    public BuildManager instance;
    public GameObject buildEffect;
    public GameObject sellEffect;

    public TurretBlueprint turretToBuild;
    public NodeController selectedNode;

    public NodeUI nodeUI;

    public bool CanBuild { get { return turretToBuild != null; } }

    public bool HasMoney
    {
        get
        {
            PlayerStatsModel playerStatsModel = GetComponent<PlayerStatsModel>();
            return playerStatsModel.Money >= turretToBuild.cost;
        }
    }

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
