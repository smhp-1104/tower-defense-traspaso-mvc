﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildManagerController : MonoBehaviour
{
    // Start is called before the first frame update



    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    public void SelectNode(NodeController node)
    {
        BuildManagerModel buildManagerModel = GetComponent<BuildManagerModel>();
        BuildManagerView buildManagerView = GetComponent<BuildManagerView>();

        if (buildManagerModel.selectedNode == node)
        {
            buildManagerView.DeselectNode();
            return;
        }

        buildManagerModel.selectedNode = node;
        buildManagerModel.turretToBuild = null;

        buildManagerModel.nodeUI.SetTarget(node);
    }
}
