﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildManagerView : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void DeselectNode()
    {
        BuildManagerModel buildManagerModel = GetComponent<BuildManagerModel>();
        buildManagerModel.selectedNode = null;
        buildManagerModel.nodeUI.Hide();
    }

    public void SelectTurretToBuild(TurretBlueprint turret)
    {
        BuildManagerModel buildManagerModel = GetComponent<BuildManagerModel>();
        buildManagerModel.turretToBuild = turret;
        DeselectNode();
    }

    public TurretBlueprint GetTurretToBuild()
    {
        BuildManagerModel buildManagerModel = GetComponent<BuildManagerModel>();
        return buildManagerModel.turretToBuild;
    }
}
