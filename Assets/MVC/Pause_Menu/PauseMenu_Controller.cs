﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class PauseMenu_Controller : MonoBehaviour
{
    public void Toggle()
    {
        PauseMenu_Model PauseMenuModel = GetComponent<PauseMenu_Model>();
        PauseMenuModel.ui.SetActive(!PauseMenuModel.ui.activeSelf);

        if (PauseMenuModel.ui.activeSelf)
        {
            Time.timeScale = 0f;
        }
        else
        {
            Time.timeScale = 1f;
        }
    }
}
