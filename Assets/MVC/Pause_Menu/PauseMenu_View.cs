﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class PauseMenu_View : MonoBehaviour
{
    

    // Update is called once per frame
    void Update()
    {
        PauseMenu_Controller PauseMenuController = GetComponent<PauseMenu_Controller>();
        if (Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.P))
        {
            PauseMenuController.Toggle();
        }
    }
    public void Retry()
    {
        PauseMenu_Model PauseMenuModel = GetComponent<PauseMenu_Model>();
        PauseMenu_Controller PauseMenuController = GetComponent<PauseMenu_Controller>();

        PauseMenuController.Toggle();
        PauseMenuModel.sceneFader.FadeTo(SceneManager.GetActiveScene().name);
    }

    public void Menu()
    {
        PauseMenu_Model PauseMenuModel = GetComponent<PauseMenu_Model>();
        PauseMenu_Controller PauseMenuController = GetComponent<PauseMenu_Controller>();
        PauseMenuController.Toggle();
        PauseMenuModel.sceneFader.FadeTo(PauseMenuModel.menuSceneName);
    }
}
