﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyView : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Healthbar(float amount)
    {
        
        EnemyModel enemyModel = GetComponent<EnemyModel>();
        enemyModel.enemyhealth -= amount;
        enemyModel.healthBar.fillAmount = enemyModel.enemyhealth / enemyModel.startHealth;
        EnemyController enemyController = GetComponent<EnemyController>();

        if (enemyModel.enemyhealth <= 0 && !enemyModel.isDead)
        {
            enemyController.Die();
            Die();
        }
    }

    public void Slowness()
    {
        EnemyController enemyController = GetComponent<EnemyController>();
        enemyController.Slow();
    }

    public void Die()
    {


        Destroy(gameObject);
    }


}
