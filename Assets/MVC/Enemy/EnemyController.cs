﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyController : MonoBehaviour
{
    // Start is called before the first frame update

    
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        EnemyModel enemyModel = GetComponent<EnemyModel>();
        if (enemyModel.isDead == true)
        {
            GameObject effect = (GameObject)Instantiate(enemyModel.deathEffect, transform.position, Quaternion.identity);
            Destroy(effect, 5f);
            Destroy(gameObject);
        }
    }
    public void Slow()
    {
        
        EnemyModel enemyModel = GetComponent<EnemyModel>();
        enemyModel.speed = enemyModel.startSpeed * (1f - enemyModel.pct); 


    }
    public void Die()
    {
        EnemyModel enemyModel = GetComponent<EnemyModel>();

        enemyModel.isDead = true;

        PlayerStatsModel playerStatsModel = GetComponent<PlayerStatsModel>();
        playerStatsModel.Money += enemyModel.worth;




        WaveSpawner.EnemiesAlive--;


    }

}
