﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOver_View : MonoBehaviour
{
    public void Retry()
    {
        GameOver_Model GameOverModel = GetComponent<GameOver_Model>();
        GameOverModel.sceneFader.FadeTo(SceneManager.GetActiveScene().name);
    }

    public void Menu()
    {
        GameOver_Model GameOverModel = GetComponent<GameOver_Model>();
        GameOverModel.sceneFader.FadeTo(GameOverModel.menuSceneName);
    }
}
