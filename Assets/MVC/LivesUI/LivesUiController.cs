﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LivesUiController : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        PlayerStatsModel playerStatsModel = GameObject.Find("GameMaster").GetComponent<PlayerStatsModel>();
        LivesUiModel livesUiModel = GetComponent<LivesUiModel>();
        livesUiModel.livesText.text = playerStatsModel.Lives.ToString() + " LIVES";
    }
}
