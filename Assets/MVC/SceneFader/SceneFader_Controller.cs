﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneFader_Controller : MonoBehaviour
{
    public IEnumerator FadeIn()
    {
        SceneFader_Model SceneFaderModel = GetComponent<SceneFader_Model>();
        float t = 1f;

        while (t > 0f)
        {
            t -= Time.deltaTime;
            float a = SceneFaderModel.curve.Evaluate(t);
            SceneFaderModel.img.color = new Color(0f, 0f, 0f, a);
            yield return 0;
        }
    }

    public IEnumerator FadeOut(string scene)
    {
        SceneFader_Model SceneFaderModel = GetComponent<SceneFader_Model>();
        float t = 0f;

        while (t < 1f)
        {
            t += Time.deltaTime;
            float a = SceneFaderModel.curve.Evaluate(t);
            SceneFaderModel.img.color = new Color(0f, 0f, 0f, a);
            yield return 0;
        }

        SceneManager.LoadScene(scene);
    }
}
