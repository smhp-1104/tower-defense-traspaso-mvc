﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class SceneFader_View : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        SceneFader_Controller SceneFaderController = GetComponent<SceneFader_Controller>();
        StartCoroutine(SceneFaderController.FadeIn());
    }

    public void FadeTo(string scene)
    {
        SceneFader_Controller SceneFaderController = GetComponent<SceneFader_Controller>();
        StartCoroutine(SceneFaderController.FadeOut(scene));
    }
}
