﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Turret_View : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("UpdateTarget", 0f, 0.5f);
    }

    // Update is called once per frame
    void Update()
    {
        Turret_Model turretModel = GetComponent<Turret_Model>();
        Turret_Controller turretController = GetComponent<Turret_Controller>();

        if (turretModel.target == null)
        {
            if (turretModel.useLaser)
            {
                if (turretModel.lineRenderer.enabled)
                {
                    turretModel.lineRenderer.enabled = false;
                    turretModel.impactEffect.Stop();
                    turretModel.impactLight.enabled = false;
                }
            }

            return;
        }

        LockOnTarget();

        if (turretModel.useLaser)
        {
            Laser();
        }
        else
        {
            if (turretModel.fireCountdown <= 0f)
            {
                Shoot();
                turretController.FireCountdown_FireRate();
            }

            turretModel.fireCountdown -= Time.deltaTime;
        }
    }

    void LockOnTarget()
    {
        Turret_Model turretModel = GetComponent<Turret_Model>();

        Vector3 dir = turretModel.target.position - transform.position;
        Quaternion lookRotation = Quaternion.LookRotation(dir);
        Vector3 rotation = Quaternion.Lerp(turretModel.partToRotate.rotation, lookRotation, Time.deltaTime * turretModel.turnSpeed).eulerAngles;
        turretModel.partToRotate.rotation = Quaternion.Euler(0f, rotation.y, 0f);
    }

    void UpdateTarget()
    {
        Turret_Model turretModel = GetComponent<Turret_Model>();

        GameObject[] enemies = GameObject.FindGameObjectsWithTag(turretModel.enemyTag);
        float shortestDistance = Mathf.Infinity;
        GameObject nearestEnemy = null;
        foreach (GameObject enemy in enemies)
        {
            float distanceToEnemy = Vector3.Distance(transform.position, enemy.transform.position);
            if (distanceToEnemy < shortestDistance)
            {
                shortestDistance = distanceToEnemy;
                nearestEnemy = enemy;
            }
        }

        if (nearestEnemy != null && shortestDistance <= turretModel.range)
        {
            turretModel.target = nearestEnemy.transform;
            turretModel.targetEnemy = nearestEnemy.GetComponent<Enemy>();
        }
        else
        {
            turretModel.target = null;
        }

    }

    void Shoot()
    {
        Turret_Model turretModel = GetComponent<Turret_Model>();

        GameObject bulletGO = (GameObject)Instantiate(turretModel.bulletPrefab, turretModel.firePoint.position, turretModel.firePoint.rotation);
        Bullet_Controller bullet = bulletGO.GetComponent<Bullet_Controller>();

        if (bullet != null)
            bullet.Seek(turretModel.target);
    }

    void Laser()
    {
        Turret_Model turretModel = GetComponent<Turret_Model>();

        turretModel.targetEnemy.TakeDamage(turretModel.damageOverTime * Time.deltaTime);
        turretModel.targetEnemy.Slow(turretModel.slowAmount);

        if (!turretModel.lineRenderer.enabled)
        {
            turretModel.lineRenderer.enabled = true;
            turretModel.impactEffect.Play();
            turretModel.impactLight.enabled = true;
        }

        turretModel.lineRenderer.SetPosition(0, turretModel.firePoint.position);
        turretModel.lineRenderer.SetPosition(1, turretModel.target.position);

        Vector3 dir = turretModel.firePoint.position - turretModel.target.position;

        turretModel.impactEffect.transform.position = turretModel.target.position + dir.normalized;

        turretModel.impactEffect.transform.rotation = Quaternion.LookRotation(dir);
    }

}
