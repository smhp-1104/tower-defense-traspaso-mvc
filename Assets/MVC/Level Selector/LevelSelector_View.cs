﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelSelector_View : MonoBehaviour
{
    public void Select(string levelName)
    {
        LevelSelector_Model levelSelectorModel = GetComponent<LevelSelector_Model>();
        levelSelectorModel.fader.FadeTo(levelName);
    }

}
