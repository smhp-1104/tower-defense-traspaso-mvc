﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelSelector_Controller : MonoBehaviour
{
    void Start()
    {
        LevelSelector_Model levelSelectorModel = GetComponent<LevelSelector_Model>();

        int levelReached = PlayerPrefs.GetInt("levelReached", 1);

        for (int i = 0; i < levelSelectorModel.levelButtons.Length; i++)
        {
            if (i + 1 > levelReached)
                levelSelectorModel.levelButtons[i].interactable = false;
        }
    }
}
