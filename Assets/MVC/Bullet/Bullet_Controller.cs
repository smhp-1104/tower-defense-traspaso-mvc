﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet_Controller : MonoBehaviour
{
    
    public void Seek(Transform _target)
    {
        Bullet_Model bulletModel = GetComponent<Bullet_Model>();
        bulletModel.target = _target;
    }

    public void HitTarget()
    {
        Bullet_Model bulletModel = GetComponent<Bullet_Model>();

        GameObject effectIns = (GameObject)Instantiate(bulletModel.impactEffect, transform.position, transform.rotation);
        Destroy(effectIns, 5f);

        if (bulletModel.explosionRadius > 0f)
        {
            Explode();
        }
        else
        {
            Damage(bulletModel.target);
        }

        Destroy(gameObject);
    }

    public void Explode()
    {
        Bullet_Model bulletModel = GetComponent<Bullet_Model>();

        Collider[] colliders = Physics.OverlapSphere(transform.position, bulletModel.explosionRadius);
        foreach (Collider collider in colliders)
        {
            if (collider.tag == "Enemy")
            {
                Damage(collider.transform);
            }
        }
    }


    public void Damage(Transform enemy)
    {
        Bullet_Model bulletModel = GetComponent<Bullet_Model>();

        EnemyView e = enemy.GetComponent<EnemyView>();

        if (e != null)
        {
            e.Healthbar(bulletModel.damage);
        }
    }


    public void OnDrawGizmosSelected()
    {
        Bullet_Model bulletModel = GetComponent<Bullet_Model>();
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, bulletModel.explosionRadius);
    }
}
