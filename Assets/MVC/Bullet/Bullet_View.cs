﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet_View : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Bullet_Model bulletModel = GetComponent<Bullet_Model>();
        Bullet_Controller bulletController = GetComponent<Bullet_Controller>();

        if (bulletModel.target == null)
        {
            Destroy(gameObject);
            return;
        }

        Vector3 dir = bulletModel.target.position - transform.position;
        float distanceThisFrame = bulletModel.speed * Time.deltaTime;

        if (dir.magnitude <= distanceThisFrame)
        {
            bulletController.HitTarget();
            return;
        }

        transform.Translate(dir.normalized * distanceThisFrame, Space.World);
        transform.LookAt(bulletModel.target);
    }
}
