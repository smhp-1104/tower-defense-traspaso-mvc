﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuModel : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        MainMenuController mainMenuController = GetComponent<MainMenuController>();
        mainMenuController.levelToLoad = "LevelSelect";
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
