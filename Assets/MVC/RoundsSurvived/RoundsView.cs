﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoundsView : MonoBehaviour
{
    // Start is called before the first frame update

    void OnEnable()
    {
        StartCoroutine(AnimateText());
    }

    void Start()
    {
        
    }

    // Update is called once per frame

    IEnumerator AnimateText()
    {
        yield return new WaitForSeconds(.7f);
        RoundsController roundsController = GetComponent<RoundsController>();
        RoundsModel roundsModel = GetComponent<RoundsModel>();
        roundsController.Rounds();

        while (roundsModel.round < PlayerStats.Rounds)
        {
            roundsModel.round++;
            roundsModel.roundsText.text = roundsModel.round.ToString();

            yield return new WaitForSeconds(.05f);
        }

    }

    void Update()
    {
        
    }
}
