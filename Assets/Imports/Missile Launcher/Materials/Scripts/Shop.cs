using UnityEngine;

public class Shop : MonoBehaviour {

	public TurretBlueprint standardTurret;
	public TurretBlueprint missileLauncher;
	public TurretBlueprint laserBeamer;

	BuildManager buildManager;

	void Start ()
	{
		buildManager = BuildManager.instance;
	}

	public void SelectStandardTurret ()
	{
        BuildManagerView buildManagerView = GameObject.Find("GameMaster").GetComponent<BuildManagerView>();
        Debug.Log("Standard Turret Selected");
		buildManagerView.SelectTurretToBuild(standardTurret);
	}

	public void SelectMissileLauncher()
	{
        BuildManagerView buildManagerView = GameObject.Find("GameMaster").GetComponent<BuildManagerView>();
        Debug.Log("Missile Launcher Selected");
        buildManagerView.SelectTurretToBuild(missileLauncher);
	}

	public void SelectLaserBeamer()
	{
        BuildManagerView buildManagerView = GameObject.Find("GameMaster").GetComponent<BuildManagerView>();
        Debug.Log("Laser Beamer Selected");
        buildManagerView.SelectTurretToBuild(laserBeamer);
	}

}
