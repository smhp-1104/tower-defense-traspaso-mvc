﻿using UnityEngine;
using UnityEngine.UI;

public class LivesUI : MonoBehaviour {

	public Text livesText;

	// Update is called once per frame
	void Update () {

        PlayerStatsModel playerStatsModel = GameObject.Find("GameMaster").GetComponent<PlayerStatsModel>();

        livesText.text = playerStatsModel.Lives.ToString() + " LIVES";
	}
}
